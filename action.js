const numbers = [5, 2, 9, 8, 10, 11, -3, 0, 22, 4, -1, 5, 2];

// 1. Реализовать перебор массива используя 
//     -for
//     -forEach
//     -map
// результатом должен быть новый массив, все числа которого на 3 больше исходных

//      FOR
const res = [];
for (let i = 0, len = numbers.length; i < len; i++) {
    res.push(numbers[i] + 3)
};
console.log(res);
//
//    forEach
const sum = [];
numbers.forEach((value, i) => sum.push(numbers[i] + 3));
console.log(sum);
//
//    MAP
const summ = numbers.map((value) => value + 3);
console.log(summ);
// ============================================================================
// 2. Получить массив чисел больше или равных 10
//     -forEach
//     -filter
// результатом должен быть новый массив

//   FILTER
const filtered = numbers.filter(myFunc);
function myFunc(value, i, arr) {
    return value >= 10;
}
console.log(filtered);

//   FOREACH
const numbers1 = [];

numbers.forEach(value => {
    if (value >= 10) {
        return numbers1.push(value);
    }
});

console.log(numbers1);

// 3. Проверить массив все ли числа в нем больше нуля (хоть одно меньше нуля). 
//    Результаты сохранить в isAllPositive, hasNegative соответственно
//     -some
//     -every

const isAllPositive = numbers.every(element => element > 0);
console.log(isAllPositive);

const hasNegative = numbers.some(element => element > 0);
console.log(hasNegative);


// 4. Посчитать сумму всех элементов массива
//     -for
//     -forEach
//     -reduce

function arraySum(array) {
    let result = 0;
    for (let i = 0; i < array.length; i++) {
        result += array[i];
    }
    console.log(result);
}
arraySum(numbers);


const a = result1();

function result1() {
    let sum = 0;
    numbers.forEach(value => sum += value);
    return sum;
}
console.log(a);


console.log(numbers.reduce((a, b) => a + b));






// 5. Отсортировать массив по убыванию
//     -sort
// результатом должен быть новый массив

const res1 = [...numbers].sort((a, b) => b - a).map(d => d * 1);
console.log(res1);


// 6. Найти максимальный элемент массива
//     -for
//     -forEach
//     -Math.max
//     -reduce

for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] > numbers[0]) {
        numbers[0] = numbers[i];
    }
}
console.log(numbers[0]);


let maxArr = 0;
numbers.forEach(function (value) {
    if (maxArr < value)
        maxArr = value;
});
console.log(maxArr);


const max = Math.max(...numbers);
console.log(max);


const bigNum = numbers.reduce((a, b) => {
    return Math.max(a, b)
});
console.log(bigNum);

// 7. Найти индекс элемента 10
//     -forEach
//     -indexOf
// результатом должен быть новый массив, все числа которого на 3 больше исходных

let copy = [];
numbers.forEach((value, i) => copy.push(numbers[10] + 3));
copy.length = 1;
console.log(copy);


let element = numbers;
let x = numbers.indexOf(10);
while (x != -1) {
    copy.push(x + 3);
    x = numbers.indexOf(element, x + 1);
}

console.log(copy, 'hello');

//



// 8. Найти элемент -3 и заменить его на число 100
//     -map
//     -indexOf + splice


// 9. Заменить все отрицательные числа на их абсолютные величины (модуль от числа)
//     -forEach + splice + Math.abs
//     -reduce + Math.abs

// 10. Найти в исходном массиве минимальное число,
//     создать массив из 10 чисел где стартовым числом будет минимальное найденное из предыдущего шага,
//     а каждое последующее на 5 больше предыдущего,
//     извлечь только четные числа,
//     найти сумму всех элементов полученного массива
